package br.com.caelum.building.api.registration;

import br.com.caelum.building.domain.Building;
import br.com.caelum.building.shared.Mapper;
import org.springframework.stereotype.Component;

@Component
class BuildingInputToBuilding implements Mapper<RegistrationController.BuildingInput, Building> {
    @Override
    public Building map(RegistrationController.BuildingInput source) {
        return new Building(source.getCondominiumName(), source.getAddress(), source.getNumberOfBlocks(), 0);
    }
}
