package br.com.caelum.building.api.describe;

import br.com.caelum.building.shared.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.MessageFormat;
import java.util.List;

import static br.com.caelum.building.api.describe.DescriberController.*;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
class DescribeService {
    private BuildingToBuildingOutput mapper;
    private DescribeRepository buildings;

    public List<BuildingOutput> listAll(List<Long> buildingIds) {
        if (CollectionUtils.isEmpty(buildingIds))
            return buildings.findAll().stream().map(mapper::map).collect(toList());

        return buildings.finaAllWhereIdsIn(buildingIds).stream().map(mapper::map).collect(toList());
    }

    public BuildingOutput showBy(Long id) {
        return buildings.findById(id).map(mapper::map).orElseThrow(() -> new NotFoundException(MessageFormat.format("Cannot find building with id: {0}", id)));
    }
}
