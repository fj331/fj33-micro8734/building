package br.com.caelum.building.listener.remote;

import br.com.caelum.building.domain.Building;
import br.com.caelum.building.shared.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

import static java.text.MessageFormat.*;

@Service
@AllArgsConstructor
public class AddApartmentService {

    private final AddApartmentRepository repository;

    public void incrementApartmentOf(Long buildingId) {
        Building building = repository.findById(buildingId)
                .orElseThrow(() ->
                        new NotFoundException(format("Cannot find building with this id {0}", buildingId)));

        building.incrementApartment();

        repository.save(building);
    }
}
