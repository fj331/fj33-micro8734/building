create table building (
    id bigint not null auto_increment,
    condominium_name varchar(255) not null,
    address varchar(255) not null ,
    number_of_blocks int not null,
    number_of_units int not null,

    constraint pk_building primary key (id)
);