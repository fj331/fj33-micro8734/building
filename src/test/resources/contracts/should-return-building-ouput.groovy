import org.springframework.cloud.contract.spec.Contract

Contract.make {

    request {
        method GET()

        urlPath('/1') {
            queryParameters {
                parameter correlationalId: 1
            }
        }

        headers {
            accept applicationJson()
        }
    }

    response {
        status OK()

        body([
                id             : 1,
                condominiumName: "Santa Julia",
                address        : "Rua vergueiro, 3185",
                numberOfBlocks : 1,
                numberOfUnits  : 13,
                realEstateAgent : "Fernando"
        ])

        headers {
            contentType applicationJson()
        }
    }


}