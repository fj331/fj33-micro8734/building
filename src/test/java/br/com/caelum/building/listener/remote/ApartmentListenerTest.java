package br.com.caelum.building.listener.remote;

import org.aspectj.weaver.patterns.IVerificationRequired;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.then;


@SpringBootTest
@AutoConfigureStubRunner(
        ids = "br.com.caelum:apartment:+:stubs:8000",
        stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class ApartmentListenerTest {

    @Autowired
    private StubTrigger trigger;

    @MockBean
    private AddApartmentService service;

    @Captor
    private ArgumentCaptor<Long> argumentCaptor;

    @Test
    void test() {
        trigger.trigger("receive_event");

        then(service).should().incrementApartmentOf(argumentCaptor.capture());


        assertEquals(35L, argumentCaptor.getValue());


    }

}